$(document).ready(function(){

    consultar_datos();

});

function consultar_datos(){
    $.post(CI_ROOT+"pedro/consultar_datos", {}, function(data, status){
        var obj = JSON.parse(data);
        console.log(obj); 
        var content = "";
        for(var i = 0; i < obj.length; i++){
            content += 
            `<tr>
                <td>${obj[i]['id_usuario']}</td>
                <td>${obj[i]['nombre']}</td>
                <td>${obj[i]['correo']}</td>
                <td>${obj[i]['telefono']}</td>
                <td>${obj[i]['usuario']}</td>
                <td>${obj[i]['pass']}</td>
                <td>${obj[i]['tipo_usuario']}</td>
                <td><button class="btn btn-danger" >borrar</button></td>
            </tr>`;
        }

        $("#tbody_usuarios").html(content);
    });
}

function crear_usuario(){
    var nombre = $("#nombre").val();
    var correo = $("#correo").val();
    var telefono = $("#telefono").val();
    var usuario = $("#usuario").val();
    var tipo = $("#tipoUsuario").val();

    $.post(CI_ROOT+"pedro/crear_usuario",
        {
            nombre : nombre,
            correo : correo,
            telefono : telefono,
            usuario : usuario,
            tipo : tipo
        }    
    , function(data, status){
            console.log(data);
        }
    );
}