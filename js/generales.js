function post(metodo,params){
    var deferred = new $.Deferred();
    //console.log(typeof(params));
    $.ajax({
        url: CI_ROOT + metodo,
        type:'POST',
        processData:false,
        contentType:false,
        cache:false,
        data: params,
        datatype:"json",
        success: function(response,textStatus,JQxhr){
            console.log(textStatus);
            deferred.resolve(response);
        },error:function(JQxhr,textStatus,errorThrown){
            swal("Alerta","Ocurrio algo inesperado, intente mas tarde","warning");
            console.log(JQxhr);
          }
        });
    return deferred.promise();
}
var formato = function(valor){
	var dia = "";
	var mes = "";
	var anio = "";
	nueva_fecha = valor.split('-');
	dia = nueva_fecha[0];
	mes = nueva_fecha[1];
	anio = nueva_fecha[2];
	return anio+"-"+mes+"-"+dia;  
}
var modalGeneral = modalGeneral || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" role="dialog" >' +
		'<div class="modal-dialog modal-lg">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 class="modalTitulo" style="margin:0;"></h3><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>' +
			'<div class="modal-body ">' +
			    '<div class="container-fluid contenido">'+
			    
			     '</div>'+
            '</div>' +
            '<div class="modal-footer">'+
                '<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>'+
                '<div class="botonClick"> </div>'+
            '</div>'+
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, contenido,guardar,size = 'lg') {
            var options;
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined' || message == null || message == "") {
				message = 'Ventana de información';
			}
			if(typeof contenido === 'undefined' || contenido == null || contenido == ""){
			  contenido = "Mensaje para el desarrollador: Debe pasa un String con elementos";  
			}
			if(typeof guardar === 'undefined' || guardar == ""){
			    guardar = "";
			    swal("Es necesario enviar funcion click");
			}else if(guardar == null){
				
			}else{
				$dialog.find('.botonClick').html('<button type="button" class="btn btn-success" onClick='+guardar+' >Guardar</button>');
			}
			var settings = $.extend({
				dialogSize: size,
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            
            // menu para verificar la opcion enviada en modalGeneral.show(null,opcion:tipo numerico);
			$dialog.find('.contenido').html(contenido);
			$dialog.find('.modalTitulo').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
			
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);
/**
 * 
 * @param {*} field campo al que se le contaran los caracteres
 * @param {*} field2 span donde se pondra el contador 
 * @param {*} maxlimit el limite de caracteres
 * @returns contador - la cantidad de caracteres escritos
 *  esta funcion se llama dentro del mismo elemento a validar, en un atributo onkeyup, ver vista audio para revisar ejemplo
 */
function textCounter(field,field2,maxlimit){
	var countfield = document.getElementById(field2);
	if ( field.value.length == maxlimit ) {
		field.value = field.value.substring( 0, maxlimit );
		return false;
	} else {
		//countfield.text = maxlimit - field.value.length;
		$("#"+field2).html(maxlimit - field.value.length);

	}
}
function cerrarSesion(){
	location.href= CI_ROOT+"principal/cerrarSesion";
}