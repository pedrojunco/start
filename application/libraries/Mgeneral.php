<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Mgeneral extends CI_Model{
    protected $CI;
    protected $id_usuario;
    public function __construct(){
            parent::__construct();
            $this->CI =& get_instance();
            $this->load->library('session');
    }
//Regresa una consulta a traves de un SP
public function multipleResult($queryString){
    if (empty($queryString)) {
                return false;
            }
    $index     = 0;
    $ResultSet = array();
		/* execute multi query */
    if (mysqli_multi_query($this->db->conn_id, $queryString)) {
        do {
            if (false != $result = mysqli_store_result($this->db->conn_id)) {
                $rowID = 0;
                while ($row = $result->fetch_assoc()) {
                    $ResultSet[$index][$rowID] = $row;
                    $rowID++;
                }
            }
            $index++;
        } while (mysqli_next_result($this->db->conn_id));
    }
    
    return (empty($ResultSet)) ? false :  $ResultSet[0];
}
public function getUser(){
    // $this->permisos = $this->session->userdata("datos_usuario")['permiso'];
	$this->id_usuario = $this->session->userdata("datos_usuario")['id_usuario'];
    return $this->id_usuario;
}
}