<?php

class M_pedro extends CI_Model {

    public function consultarUsuarios(){
        $this->db->select('*');
        $this->db->from('usuarios');
        $result = $this->db->get();

        return $result->result_array();//row_array();
    }
    
    public function crear_usuario( $nombre, $correo, $telefono, $usuario, $tipo){
        $data = array(
            'nombre' => $nombre ,
            'correo' => $correo ,
            'telefono' => $telefono ,
            'usuario' => $usuario ,
            'tipo_usuario' => $tipo
         );
         
        $result = $this->db->insert('usuarios', $data);

        if($result){
            return "1";
        }else{
            return "0";
        }
    }

}