<link rel="stylesheet" href="<?php echo base_url();?>node_modules/bootstrap/dist/css/bootstrap.css">

<script src="<?php echo base_url();?>node_modules/jquery/dist/jquery.js"></script>
<script src="<?php echo base_url();?>node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>js/pedro.js"></script>
<script>
    var CI_ROOT = '<?php echo base_url();?>';
</script>
<div class="container"><br><br>
    <div class="row">
        <h3>Crear usuario</h3>
    </div>
    <div class="row">
        <div class="col-4">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Nombre</span>
                </div>
                <input type="text" class="form-control" id="nombre"> 
            </div>
        </div>
        <div class="col-4">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">correo</span>
                </div>
                <input type="text" class="form-control" id="correo">
            </div>
        </div>
        <div class="col-4">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Teléfono</span>
                </div>
                <input type="text" class="form-control" id="telefono">
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-4">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">usuario</span>
                </div>
                <input type="text" class="form-control" id="usuario">
            </div>
        </div>
        <div class="col-4">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Tipo usuario</span>
                </div>
                <input type="text" class="form-control" id="tipoUsuario">
            </div>
        </div>
        <div class="col-2">
            <button type="button" class="btn btn-primary" onclick="crear_usuario();">Primary</button>
        </div>   
    </div>
    
    <div class="col-12">
        <h3>Tabla de usuarios</h3>
    </div>
    <div class="col-12 table-responisve">
        <table class="table table-bordered table-striped table-condensed">
            <thead>
                <th>No.</th>
                <th>nombre</th>
                <th>correo</th>
                <th>telefono</th>
                <th>usuario</th>
                <th>pass</th>
                <th>tipo usuario</th>
                <th>acciones</th>
            </thead>
            <tbody id="tbody_usuarios">
               
            </tbody>
        </table>    
    </div>
</div>


