<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog</title>
    <style>
        /* . Clase, # id*/
        .titulo{
            font-size: 60px;
            color: #00b4ff;
        }
        .contenido{
            font-size: 40px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="titulo">
        <?php echo $titulo;?>
    </div>
    <div class="contenido">
        <?php echo $contenido;?>
    </div>
</body>
</html>