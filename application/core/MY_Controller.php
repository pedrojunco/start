<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General extends CI_Controller {
    protected $info_usuario;
	protected $layout='default';
	protected $configuraciones;
	protected $show_layout = true;
	protected $permisos;
	protected $id_usuario;
    
    public function __construct(){
            parent::__construct();
			$this->load->library('session');
			$login = $this->verificar_sesion();
			if($login === FALSE){
				redirect('login');
			}else{
				$this->permisos = $this->session->userdata("datos_usuario")['permiso'];
				$this->id_usuario = $this->session->userdata("datos_usuario")['id_usuario'];
			}
    }
    protected function set_layout($nuevo_layout = NULL){
		if($nuevo_layout != NULL)
			$this->layout = $nuevo_layout;
		$show_layout = TRUE;
	}
    protected function view($vista, $param = NULL, $view_as_data= FALSE){
        $view_user_data = array();
		//se envia el permiso a la visa para un mejor control
		if($this->show_layout == true){
			$data = array('vista' => $vista, 'vista_params' => $param, 'view_user_data' => $view_user_data,);
			return $this->load->view('/layout_index/'.$this->layout, $data, $view_user_data);
		}else{
			return $this->load->view('/vistas/'.$vista, $param, $datos);
		}
    }
    protected function verificar_sesion(){
		if ($this->session->userdata('login_state') !== FALSE){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	function subirImagen($archivo,$ruta,$nombreImagen){
		$Destino = "";
		$bandera= FALSE;
		$temporal="";
		if (count($archivo) > 0) {
		  foreach ($archivo as $key){
			   if($key['error'] == UPLOAD_ERR_OK ){
				 if(file_exists(strtolower($ruta.$nombreImagen))){
		   			$nombreImagen = strtolower($nombreImagen);
				 }else{
				  // $nombreImagen = strtolower($key['name']);
				   $temporal = $key['tmp_name']; //Obtenemos la ruta Original del archivo
				   $Destino = $ruta.$nombreImagen;
					move_uploaded_file($temporal,$Destino);
				 }
			   }
			 }
			 if($nombreImagen == ""){
				return FALSE;
			 }else{
				return $nombreImagen;
			 }
			 
		}else{
			return FALSE;
		}

	}
	function subirPerfil($ruta,$imagen){
		$filteredData = explode(',', $imagen);
		$unencoded = base64_decode($filteredData[1]);
		$fp = fopen('./'.$ruta, 'w+');
        fwrite($fp, $unencoded);
		return fclose($fp);
		
	}
	function mensaje($titulo,$texto,$alerta){
		$mensaje = array(
			"titulo" => $titulo,
			"texto" => $texto,
			"alerta" => $alerta
		);
		return json_encode($mensaje);
	}

	function response($data, $estatus = 200){
        $this->output->set_status_header($estatus);
        $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
    }
}