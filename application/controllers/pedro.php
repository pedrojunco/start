<?php

class Pedro extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_pedro','', TRUE);
    }

    function index(){
        $usuarios = $this->m_pedro->consultarUsuarios();
        $this->load->view('pedro/usuarios', array("usuarios"=>$usuarios));
    }

    public function consultar_datos(){
        $usuarios = $this->m_pedro->consultarUsuarios();

        echo json_encode($usuarios);
    }

    public function crear_usuario(){
        $nombre = $this->input->post('nombre');
        $correo = $this->input->post('correo');
        $telefono = $this->input->post('telefono');
        $usuario = $this->input->post('usuario');
        $tipo = $this->input->post('tipo');
        
        $respuesta = $this->m_pedro->crear_usuario( $nombre, $correo, $telefono, $usuario, $tipo);

        echo $respuesta;
    }
}// fin clase